package helper

import (
	"github.com/google/uuid"
)

func ManyStringToUUID(idsString []string) ([]uuid.UUID, *error) {
	idsUuid := []uuid.UUID{}
	for _, ids := range idsString {
		idParsed, errIdParsed := uuid.Parse(ids)
		if errIdParsed != nil {
			return nil, &errIdParsed
		}
		idsUuid = append(idsUuid, idParsed)
	}

	return idsUuid, nil
}
