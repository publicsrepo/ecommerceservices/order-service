CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE IF NOT EXISTS addresses (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP null,
    deleted_at TIMESTAMP null,
    user_id UUID NOT NULL,
    street character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    province character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    country character varying(255) NOT NULL
) ;

CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP null,
    deleted_at TIMESTAMP null,
    name character varying(255) NOT NULL,
  	type character varying(10) NOT NULL,
    password  character varying(255) NOT NULL,
    email  character varying(255) NOT NULL,
    token  character varying(255) NULL
);
