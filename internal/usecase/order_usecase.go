package usecase

import (
	"fmt"
	"order-services/internal/entity"
	"order-services/internal/model"
	"order-services/internal/model/payload"
	"order-services/internal/model/transformers"
	"order-services/internal/repository"
	"order-services/pkg/errs"
	"order-services/pkg/helper"
	helpers "order-services/pkg/helper"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type OrderUseCase struct {
	DB              *gorm.DB
	Log             *logrus.Logger
	Validate        *validator.Validate
	OrderRepository *repository.OrderRepository
	Config          *viper.Viper
	//OrderProducer   *messaging.OrderProducer
}

func NewOrderUseCase(db *gorm.DB, logger *logrus.Logger, validate *validator.Validate, Config *viper.Viper, orderRepository *repository.OrderRepository) *OrderUseCase {
	return &OrderUseCase{
		DB:              db,
		Log:             logger,
		Validate:        validate,
		OrderRepository: orderRepository,
		Config:          Config,
	}
}

func (c *OrderUseCase) CreateOrder(fiberCtx *fiber.Ctx, request *model.CreateOrderRequest) (*model.GetOrderDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()

	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	err := c.Validate.Struct(request)
	if err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idProductParsed, errIdParsed := uuid.Parse(request.ProductId)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}
	idShopParsed, errIdParsed := uuid.Parse(request.ShopId)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}
	idUserParsed, errIdParsed := uuid.Parse(request.UserId)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}
	idWarehouseParsed, errIdParsed := uuid.Parse(request.WarehouseId)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	now := helper.GetCurrentTimeWithUTC()
	order := &entity.Order{
		BaseEntity: entity.BaseEntity{
			Id:        uuid.New(),
			CreatedAt: now,
			UpdatedAt: nil,
			DeletedAt: nil,
		},
		ProductId:     idProductParsed,
		ProductName:   nil,
		UserId:        idUserParsed,
		ShopId:        idShopParsed,
		ShopName:      nil,
		WarehouseId:   idWarehouseParsed,
		WarehouseName: nil,
	}

	if err := c.OrderRepository.Create(tx, order); err != nil {
		desc := fmt.Sprintf("Failed create order to database : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetOrderDetailToResponse(order), nil
}

func (c *OrderUseCase) ChangeStatusOrder(fiberCtx *fiber.Ctx, request *model.ChangeStatusOrderRequest) (*model.GetOrderDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	order := new(entity.Order)
	if err := c.OrderRepository.FindById(tx, order, idParsed); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusNotFound).
			SetMessage(fiber.StatusNotFound).
			SetDescription(desc)
	}
	now := time.Now()

	order.Status = request.Status
	order.UpdatedAt = &now

	if err := c.OrderRepository.Update(tx, order); err != nil {
		desc := fmt.Sprintf("Failed save order : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction: %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetOrderDetailToResponse(order), nil
}

func (c *OrderUseCase) DeleteOrder(fiberCtx *fiber.Ctx, request *model.DeleteOrderRequest) (bool, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idsUuid, errIdParsed := helpers.ManyStringToUUID(request.Ids)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(*errIdParsed)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	orders, err := c.OrderRepository.GetListOrder(tx, payload.GetListOrder{Id: idsUuid})
	if err != nil {
		desc := fmt.Sprintf("Failed find order by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	timeNow := helper.GetCurrentTimeWithUTC()
	for _, u := range orders {
		u.DeletedAt = &timeNow
		if err := c.OrderRepository.Update(tx, &u); err != nil {
			desc := fmt.Sprintf("Failed update order : %+v", err)
			c.Log.WithError(err).Warnf(desc)
			return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
				SetMessage(fiber.StatusInternalServerError).
				SetDescription(desc)
		}
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)

	}

	// event := transformers.OrderToEvent(order)
	// c.Log.Info("Publishing order created event")
	// if err := c.OrderProducer.Send(event); err != nil {
	// 	c.Log.Warnf("Failed publish order created event : %+v", err)
	// 	return nil, fiber.ErrInternalServerError
	// }

	return true, nil
}

func (c *OrderUseCase) GetDetailOrder(fiberCtx *fiber.Ctx, request *model.GetOrderDetailRequest) (*model.GetOrderDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	uuidParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	order := entity.Order{}
	if err := c.OrderRepository.FindById(tx, &order, uuidParsed); err != nil {
		desc := fmt.Sprintf("Failed find order by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetOrderDetailToResponse(&order), nil
}

func (c *OrderUseCase) GetListOrder(fiberCtx *fiber.Ctx, request *model.GetListParamsRequest) ([]model.GetOrderDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	orders, err := c.OrderRepository.GetListOrder(tx, payload.GetListOrder{
		GetListParamsRequest: *request,
	})
	if err != nil {
		desc := fmt.Sprintf("Failed find order by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetOrderListToResponse(orders), nil
}
