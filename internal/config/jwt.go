package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type JwtProps struct {
	SecretKey string
}

func NewAuthJwt(config *viper.Viper, log *logrus.Logger) *JwtProps {
	secretKey := config.GetString("jwt.secret_key")

	if len(secretKey) == 0 {
		log.Fatalf("secret key is not exist in config")
	}

	return &JwtProps{
		SecretKey: secretKey,
	}

}
