package config

import (
	"order-services/internal/delivery/http"
	"order-services/internal/delivery/http/middleware"
	"order-services/internal/delivery/http/route"
	"order-services/internal/repository"
	"order-services/internal/usecase"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type BootstrapConfig struct {
	DB       *gorm.DB
	App      *fiber.App
	Log      *logrus.Logger
	Validate *validator.Validate
	Config   *viper.Viper
}

func Bootstrap(config *BootstrapConfig) {
	// setup repositories
	orderRepository := repository.NewOrderRepository(config.Log)

	// setup use cases
	orderUseCase := usecase.NewOrderUseCase(config.DB, config.Log, config.Validate, config.Config, orderRepository)

	// setup controller
	orderController := http.NewOrderController(orderUseCase, config.Log)

	// setup middleware
	authMiddleware := middleware.NewAuth(orderUseCase)
	routeConfig := route.RouteConfig{
		App:             config.App,
		OrderController: orderController,
		AuthMiddleware:  authMiddleware,
	}
	routeConfig.Setup()
}
