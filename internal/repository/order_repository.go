package repository

import (
	"order-services/internal/entity"
	"order-services/internal/model/payload"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type OrderRepository struct {
	Repository[entity.Order]
	Log *logrus.Logger
}

func NewOrderRepository(log *logrus.Logger) *OrderRepository {
	return &OrderRepository{
		Log: log,
	}
}

func (r *OrderRepository) GetListOrder(db *gorm.DB, payloads payload.GetListOrder) ([]entity.Order, error) {
	Order := []entity.Order{}
	q := db.Where("deleted_at is NULL")

	if len(payloads.Id) > 0 {
		q = q.Where("id in (?)", payloads.Id)
	}
	if len(payloads.Category) > 0 {
		q = q.Where("category in (?)", payloads.Category)
	}
	if len(payloads.ShopId) > 0 {
		q = q.Where("shop_id in (?)", payloads.ShopId)
	}
	err := q.Find(&Order).Error
	if err != nil {
		return nil, err
	}
	return Order, nil
}
