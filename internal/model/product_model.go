package model

// response
type GetOrderDetailResponse struct {
	BaseResponse
	ProductId   string `json:"product_id"`
	ProductName string `json:"product_name"`
	UserId      string `json:"user_id"`
	ShopId      string `json:"shop_id"`
	ShopName    string `json:"shop_name"`
	WarehouseId string `json:"warehouse_id"`
}

// request
type CreateOrderRequest struct {
	ProductId   string `json:"product_id" validate:"required"`
	UserId      string `json:"user_id" validate:"required"`
	ShopId      string `json:"shop_id" validate:"required"`
	WarehouseId string `json:"warehouse_id" validate:"required"`
}

type ChangeStatusOrderRequest struct {
	ID     string `json:"id" validate:"required,uuid"`
	Status string `json:"status" validate:"required"`
}

type DeleteOrderRequest struct {
	Ids []string `json:"order_ids" validate:"required"`
}

type GetOrderDetailRequest struct {
	ID string `json:"order_ids" validate:"required,max=100"`
}
