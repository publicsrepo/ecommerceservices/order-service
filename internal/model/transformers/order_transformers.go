package transformers

import (
	"order-services/internal/entity"
	"order-services/internal/model"
)

func GetOrderDetailToResponse(order *entity.Order) *model.GetOrderDetailResponse {
	result := model.GetOrderDetailResponse{}
	if order == nil {
		return nil
	}

	result.BaseResponse = model.BaseResponse{
		ID:        order.Id.String(),
		CreatedAt: order.CreatedAt,
		UpdatedAt: *order.UpdatedAt,
	}

	result.ProductId = order.ProductId.String()
	result.ProductName = *order.ProductName
	result.UserId = order.UserId.String()
	result.ShopId = order.ShopId.String()
	result.ShopName = *order.ShopName
	result.WarehouseId = order.WarehouseId.String()

	return &result
}

func GetOrderListToResponse(order []entity.Order) []model.GetOrderDetailResponse {
	finalResult := []model.GetOrderDetailResponse{}

	for _, p := range order {
		finalResult = append(finalResult, model.GetOrderDetailResponse{
			BaseResponse: model.BaseResponse{
				ID:        p.Id.String(),
				CreatedAt: p.CreatedAt,
				UpdatedAt: *p.UpdatedAt,
			},
			ProductId:   p.ProductId.String(),
			ProductName: *p.ProductName,
			UserId:      p.UserId.String(),
			ShopId:      p.ShopId.String(),
			ShopName:    *p.ShopName,
			WarehouseId: p.WarehouseId.String(),
		})
	}
	return finalResult
}
