package payload

import (
	"order-services/internal/model"

	"github.com/google/uuid"
)

type GetListOrder struct {
	Id       []uuid.UUID
	Category []string
	ShopId   []uuid.UUID
	model.GetListParamsRequest
}
