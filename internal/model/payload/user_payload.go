package payload

import (
	"order-services/internal/model"

	"github.com/google/uuid"
)

type GetListUser struct {
	Id    []uuid.UUID
	Email []string
	model.GetListParamsRequest
}
