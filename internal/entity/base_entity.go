package entity

import (
	"time"

	"github.com/google/uuid"
)

type BaseEntity struct {
	Id        uuid.UUID  `gorm:"column:id;type:uuid;primaryKey"`
	CreatedAt time.Time  `gorm:"column:created_at;autoCreateTime:milli;autoUpdateTime:milli"`
	UpdatedAt *time.Time `gorm:"column:updated_at;autoCreateTime:milli;autoUpdateTime:milli"`
	DeletedAt *time.Time `gorm:"column:deleted_at;default:null"`
}
