package entity

import "github.com/google/uuid"

type Order struct {
	BaseEntity
	ProductId     uuid.UUID `gorm:"column:product_id"`
	ProductName   *string   `gorm:"column:product_name"`
	UserId        uuid.UUID `gorm:"column:user_id"`
	ShopId        uuid.UUID `gorm:"column:shop_id"`
	ShopName      *string   `gorm:"column:shop_name"`
	WarehouseId   uuid.UUID `gorm:"column:warehouse_id"`
	WarehouseName *string   `gorm:"column:warehouse_name"`
	Status        string    `gorm:"column:status"`
	PaymentMethod string    `gorm:"column:payment_method"`
}

func (a *Order) TableName() string {
	return "order"
}
