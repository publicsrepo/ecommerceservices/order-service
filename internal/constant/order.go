package constant

var (
	OrderStatusCheckout            = "CHECKOUT"
	OrderStatusWaitingPayment      = "WAITING_FOR_PAYMENT"
	OrderStatusWaitingShopApproval = "WAITING_CONFIRM_SHOP"
	OrderStatusWaitingCourier      = "WAITING_TAKE_COURIER"
	OrderStatusSending             = "SENDING"
	OrderStatusDelivered           = "DELIVERED"
	OrderStatusFinished            = "FINISHED"
	OrderStatusCancelled           = "CANCELLED"
	OrderStatusPaymentFailed       = "PAYMENT_FAILED"
)
