package middleware

import (
	"order-services/internal/usecase"

	"github.com/gofiber/fiber/v2"
)

func NewAuth(userUserCase *usecase.OrderUseCase) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		// request := &model.VerifyUserRequest{Token: ctx.Get("Authorization", "NOT_FOUND")}
		// userUserCase.Log.Debugf("Authorization : %s", request.Token)
		// auth, err := userUserCase.Verify(ctx, request)
		// if err != nil {
		// 	userUserCase.Log.Warnf("Failed find user by token : %+v", err)
		// 	return fiber.ErrUnauthorized
		// }
		// userUserCase.Log.Debugf("User : %+v", auth.ID)
		// ctx.Locals("auth", auth)
		return ctx.Next()
	}
}
