package route

import (
	"order-services/internal/delivery/http"

	"github.com/gofiber/fiber/v2"
)

type RouteConfig struct {
	App             *fiber.App
	OrderController *http.OrderController
	AuthMiddleware  fiber.Handler
}

func (c *RouteConfig) Setup() {
	c.SetupConsoleRoute()
}

func (c *RouteConfig) SetupConsoleRoute() {
	c.App.Use(c.AuthMiddleware)

	c.App.Post("/api/v1/order-management/order", c.OrderController.CreateOrder)
	c.App.Put("/api/v1/order-management/order-status", c.OrderController.ChangeStatusOrder)
	c.App.Get("/api/v1/order-management/orders", c.OrderController.GetListOrder)
	c.App.Get("/api/v1/order-management/order/:order_id", c.OrderController.GetDetailOrder)

	c.App.Get("/api/v1/order-management/order/:order_id", c.OrderController.GetDetailOrder)

}
