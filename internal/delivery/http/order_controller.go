package http

import (
	"order-services/internal/delivery/http/middleware"
	"order-services/internal/model"
	"order-services/internal/usecase"
	"order-services/pkg/errs"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type OrderController struct {
	UseCaseOrder *usecase.OrderUseCase
	Log          *logrus.Logger
	Config       *viper.Viper
}

func NewOrderController(UseCaseOrder *usecase.OrderUseCase, log *logrus.Logger) *OrderController {
	return &OrderController{
		Log:          log,
		UseCaseOrder: UseCaseOrder,
	}
}

func (c *OrderController) CreateOrder(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetOrderDetailResponse]{
		Status: "FAILED",
	}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)

	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}
	request := new(model.CreateOrderRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}

	response, err := c.UseCaseOrder.CreateOrder(ctx, request)
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Data = response
	finalResult.Status = "SUCCESS"
	return ctx.JSON(finalResult)
}

func (c *OrderController) ChangeStatusOrder(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetOrderDetailResponse]{Status: "FAILED"}
	request := new(model.ChangeStatusOrderRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}
	response, err := c.UseCaseOrder.ChangeStatusOrder(ctx, request)
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}

func (c *OrderController) DeleteOrder(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[bool]{Status: "FAILED"}
	request := new(model.DeleteOrderRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}

	response, err := c.UseCaseOrder.DeleteOrder(ctx, request)
	if response && err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}

func (c *OrderController) GetListOrder(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*[]model.GetOrderDetailResponse]{Status: "FAILED"}

	var (
		limitFilter  *int = nil
		offsetFilter *int = nil
	)
	limit := ctx.Query("limit")
	offset := ctx.Query("offset")

	if len(limit) > 0 {
		limitStr, errlimit := strconv.Atoi(limit)
		if errlimit != nil {
			c.Log.WithError(errlimit)
			finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusBadRequest).SetDescription("Invalid Limit Params")
			return ctx.JSON(finalResult)
		}
		limitFilter = &limitStr
	}
	if len(offset) > 0 {
		offsetStr, erroffset := strconv.Atoi(offset)
		if erroffset != nil {
			c.Log.WithError(erroffset)
			finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusBadRequest).SetDescription("Invalid Offset Params")
			return ctx.JSON(finalResult)
		}
		offsetFilter = &offsetStr
	}

	orderBy := ctx.Query("orderBy")
	order := ctx.Query("order")
	search := ctx.Query("search")

	response, err := c.UseCaseOrder.GetListOrder(ctx, &model.GetListParamsRequest{
		Search:  search,
		Order:   order,
		OrderBy: orderBy,
		Limit:   limitFilter,
		Offset:  offsetFilter,
	})
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}

	finalResult.Status = "SUCCESS"
	finalResult.Data = &response
	return ctx.JSON(finalResult)
}

func (c *OrderController) GetDetailOrder(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetOrderDetailResponse]{Status: "FAILED"}
	userId := ctx.Params("user_id")
	response, err := c.UseCaseOrder.GetDetailOrder(ctx, &model.GetOrderDetailRequest{ID: userId})
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}
